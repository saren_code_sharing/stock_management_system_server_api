package com.dinsaren.stockmanagementsystemserverapi.services;

import com.dinsaren.stockmanagementsystemserverapi.models.FileImageDetail;
import com.dinsaren.stockmanagementsystemserverapi.models.res.UploadImageRes;
import org.springframework.web.multipart.MultipartFile;

public interface UploadFileService {
    UploadImageRes uploadFile(MultipartFile files);

    void updateImage(String oldImage, String newImage);

    FileImageDetail findImageByFileName(String fileName);
}
