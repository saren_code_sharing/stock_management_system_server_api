package com.dinsaren.stockmanagementsystemserverapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StockManagementSystemServerApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(StockManagementSystemServerApiApplication.class, args);
    }

}
