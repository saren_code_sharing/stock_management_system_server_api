package com.dinsaren.stockmanagementsystemserverapi.payload.request;

public class LogOutReq {
  private Integer userId;

  public Integer getUserId() {
    return this.userId;
  }
}
