package com.dinsaren.stockmanagementsystemserverapi.payload.request;


import lombok.Data;

@Data
public class RegisterReq {
    private String firstName;
    private String lastName;
    private String username;
    private String email;
    private String phoneNumber;
    private String password;
    private String confirmPassword;
    private String dob;
}
