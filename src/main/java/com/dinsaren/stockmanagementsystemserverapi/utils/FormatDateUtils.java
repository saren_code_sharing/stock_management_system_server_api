package com.dinsaren.stockmanagementsystemserverapi.utils;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatDateUtils {
    public static final String yyyyddMM = "yyyy/dd/MM";
    public static final String yyyy_dd_MM = "yyyy-dd-MM";
    public static Date convertStringToDate(String stringDate) {
        try {
            Date date = new SimpleDateFormat(yyyyddMM).parse(stringDate);
            return date;
        }catch (ParseException e){
            return new Date();
        }
    }


    public static Date convertStringToDateYyyyDdMm(String stringDate) {
        try {
            Date date = new SimpleDateFormat(yyyy_dd_MM).parse(stringDate);
            return date;
        }catch (ParseException e){
            return new Date();
        }
    }

}
