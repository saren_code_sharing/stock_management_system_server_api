package com.dinsaren.stockmanagementsystemserverapi.common;

import org.apache.tomcat.util.codec.binary.Base64;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.*;

public class AESCipherEncrypter {
    private static final String AES_KEY = "HG58YZ3CR9";

    private static final String ONE_SIGNAL_AES_KEY = "IK69ZA4DS0";

    public static String encrypt(String orignalText) {

        String encodedText = null;

        try {
            final MessageDigest md = MessageDigest.getInstance("SHA-256");
            final byte[] ase = AES_KEY.getBytes("utf-8");
            final byte[] digestOfPassword = md.digest(AES_KEY.getBytes("utf-8"));
            final SecretKey key = new SecretKeySpec(digestOfPassword, "AES");

            final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            final IvParameterSpec iv = new IvParameterSpec(new byte[16]);

            cipher.init(Cipher.ENCRYPT_MODE, key, iv);

            final byte[] plainTextBytes = orignalText.getBytes("utf-8");
            final byte[] encodeTextBytes = cipher.doFinal(plainTextBytes);
            final byte[] encodeTextBytes1 = cipher.doFinal(encodeTextBytes);

            encodedText = new Base64().encodeToString(encodeTextBytes);


        } catch (NoSuchAlgorithmException |
                 UnsupportedEncodingException |
                 IllegalBlockSizeException |
                 InvalidKeyException |
                 BadPaddingException |
                 NoSuchPaddingException | InvalidAlgorithmParameterException e) {

        }

        return encodedText;
    }

    public static String encryptWithKey(String orignalText, String strKey) throws Throwable {

        String encodedText = null;

        try {
            final MessageDigest md = MessageDigest.getInstance("SHA-256");
            final byte[] ase = strKey.getBytes("utf-8");
            final byte[] digestOfPassword = md.digest(strKey.getBytes("utf-8"));
            final SecretKey key = new SecretKeySpec(digestOfPassword, "AES");

            final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            final IvParameterSpec iv = new IvParameterSpec(new byte[16]);

            cipher.init(Cipher.ENCRYPT_MODE, key, iv);

            final byte[] plainTextBytes = orignalText.getBytes("utf-8");
            final byte[] encodeTextBytes = cipher.doFinal(plainTextBytes);

            encodedText = new Base64().encodeToString(encodeTextBytes);


        } catch (NoSuchAlgorithmException |
                 UnsupportedEncodingException |
                 IllegalBlockSizeException |
                 InvalidKeyException |
                 BadPaddingException |
                 NoSuchPaddingException | InvalidAlgorithmParameterException e) {

            throw new Throwable(e);
        }

        return encodedText;
    }

    public static String encryptContentSMS(String orignalText) throws Throwable {

        String encodedText = null;

        try {
            final MessageDigest md = MessageDigest.getInstance("SHA-256");
            final byte[] ase = ONE_SIGNAL_AES_KEY.getBytes("utf-8");
            final byte[] digestOfPassword = md.digest(ONE_SIGNAL_AES_KEY.getBytes("utf-8"));
            final SecretKey key = new SecretKeySpec(digestOfPassword, "AES");

            final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            final IvParameterSpec iv = new IvParameterSpec(new byte[16]);

            cipher.init(Cipher.ENCRYPT_MODE, key, iv);

            final byte[] plainTextBytes = orignalText.getBytes("utf-8");
            final byte[] encodeTextBytes = cipher.doFinal(plainTextBytes);

            encodedText = new Base64().encodeToString(encodeTextBytes);

        } catch (NoSuchAlgorithmException | UnsupportedEncodingException | IllegalBlockSizeException
                 | InvalidKeyException | BadPaddingException | NoSuchPaddingException
                 | InvalidAlgorithmParameterException e) {
            throw new Throwable(e);
        }

        return encodedText;
    }

    public static String decrypt(String orignalText) throws Throwable {

        try {
            final MessageDigest md = MessageDigest.getInstance("SHA-256");
            final byte[] digestOfPassword = md.digest(AES_KEY.getBytes("utf-8"));
            final SecretKey key = new SecretKeySpec(digestOfPassword, "AES");

            final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

            cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(new byte[16]));
            final byte[] plainTextBytes = Base64.decodeBase64(orignalText);
            final byte[] encodeTextBytes = cipher.doFinal(plainTextBytes);

            return new String(encodeTextBytes);

        } catch (NoSuchAlgorithmException | UnsupportedEncodingException | IllegalBlockSizeException
                 | InvalidKeyException | BadPaddingException | NoSuchPaddingException
                 | InvalidAlgorithmParameterException e) {
            throw new Throwable(e);
        }
    }

    public static String decryptWithKey(String orignalText, String strKey) throws Throwable {

        try {
            final MessageDigest md = MessageDigest.getInstance("SHA-256");
            final byte[] digestOfPassword = md.digest(strKey.getBytes("utf-8"));
            final SecretKey key = new SecretKeySpec(digestOfPassword, "AES");

            final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

            cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(new byte[16]));
            final byte[] plainTextBytes = Base64.decodeBase64(orignalText);
            final byte[] encodeTextBytes = cipher.doFinal(plainTextBytes);

            return new String(encodeTextBytes);

        } catch (NoSuchAlgorithmException | UnsupportedEncodingException | IllegalBlockSizeException
                 | InvalidKeyException | BadPaddingException | NoSuchPaddingException
                 | InvalidAlgorithmParameterException e) {
            throw new Throwable(e);
        }
    }

    public static void main(String[] args) throws GeneralSecurityException {
        try {
            System.out.println("Core : " + AESCipherEncrypter.encrypt("696192"));
            System.out.println("KEY" + AESCipherEncrypter.encryptWithKey("{\"partner_id\":\"SMILE SHOP\",\"partner_reference_number\":\"PREORD0000001\",\"amount\":12.5,\"currency\":\"USD\",\"product_detail\":{\"product_id\":\"PRO1221\",\"product_type\":\"TYPE_HEALTH\",\"product_name\":\"Medecine\",\"transaction_type\":\"PAYMENT\"}}", "391945f1cd86cc0956c8605e12a78ba390c090c58251e9c8916ec226c16e4705"));
            System.out.println("Decrypt " + AESCipherEncrypter.decryptWithKey("IdQo2EIf209e0b1btzmmCvOsPxnieBOeOkuErEem18654TQNPKv3y+WU81IrQRgpqv71Tq/X/43tNsl/bbMKTkXuORMNI0iSsGj4Lj6q/8N3rENYU/ddzom6VMRSzCrP6hoAKasabFN5YXtR5OX7sTYQpL1X1+eBhDicRMW6LxSv9C1KJ7qvJxwpnb3D906JJvKaaZMRLHau+izt6/0blA==", "391945f1cd86cc0956c8605e12a78ba390c090c58251e9c8916ec226c16e4705"));
        } catch (Throwable e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

}