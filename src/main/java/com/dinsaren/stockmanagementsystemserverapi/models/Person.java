package com.dinsaren.stockmanagementsystemserverapi.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "sms_person")
@DynamicUpdate()
@Data
public class Person extends BaseEntity {
    private static final long serialVersionUID = 4489397646584896516L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String phoneNumber;
    private String email;
    private String address;
    private String remark;
    private String personType;
    private String status;
    private String checkDefault;

}