package com.dinsaren.stockmanagementsystemserverapi.models.req;

import lombok.Data;

import java.util.List;

@Data
public class TransactionReq {
    private Integer id;
    private Double totalItems;
    private Double totalAmount;
    private Double discount;
    private int customerId;
    private String transactionType;
    private int bankId;
    private String status;
    private String transactionSession;
    public List<TransactionItemReq> items;

}
