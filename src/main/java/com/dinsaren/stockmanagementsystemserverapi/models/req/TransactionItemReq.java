package com.dinsaren.stockmanagementsystemserverapi.models.req;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TransactionItemReq {
    private int id;
    private int productId;
    private int unitTypeId;
    private int transactionId;
    private int qty;
    private Double price;
    private Double cost;
    private Double discount;
    private String status;
}
