package com.dinsaren.stockmanagementsystemserverapi.models.req;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class GetTransactionReq {
    private Integer id;
    private String transactionType;
    private Integer mainTransactionId;
    private String status;
    private String transactionSession;
    private int page;
    private int limit;
    private int totalPage;
    private String startDate;
    private String endDate;
    private String type;
}
