package com.dinsaren.stockmanagementsystemserverapi.models;

import com.dinsaren.stockmanagementsystemserverapi.constants.Constants;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;

@Data
@MappedSuperclass
public class BaseEntity implements Serializable {
    private static final long serialVersionUID = -8892838641805537110L;
    @Column(name = "CREATED_AT")
    private Date createAt = new Date();
    @Column(name = "CREATED_BY")
    private String createBy = Constants.SYSTEM;
    @Column(name = "UPDATED_AT")
    private Date updateAt;
    @Column(name = "UPDATED_BY")
    private String updateBy;

}
