package com.dinsaren.stockmanagementsystemserverapi.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "sms_product_group_detail")
@DynamicUpdate()
@Data
public class ProductGroupDetail extends BaseEntity {
    private static final long serialVersionUID = 4489397646584896516L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    @JoinColumn(name = "product_unit_id")
    private ProductUnit productUnit;
    @ManyToOne
    @JoinColumn(name = "product_group_id")
    private ProductGroup productGroup;

    private String status;
}