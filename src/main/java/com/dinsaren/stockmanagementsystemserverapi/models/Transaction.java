package com.dinsaren.stockmanagementsystemserverapi.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "sms_transaction")
@DynamicUpdate()
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Transaction extends BaseEntity {
    private static final long serialVersionUID = 4489397646584896516L;
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Double totalItems;
    private Double totalAmount;
    private Double payableAmount;
    private Double discount;
    @ManyToOne
    @JoinColumn(name = "person_id")
    private Person person;
    private String transactionType;//Order or Purchase
    @ManyToOne
    @JoinColumn(name = "bank_id")
    private Bank bank;
    private String status;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @ManyToOne
    @JoinColumn(name = "exchange_rate_id")
    private ExchangeRate exchangeRate;
    private String transactionSession;

}