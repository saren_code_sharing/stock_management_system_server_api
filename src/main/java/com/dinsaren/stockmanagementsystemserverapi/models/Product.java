package com.dinsaren.stockmanagementsystemserverapi.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "sms_product")
@DynamicUpdate()
@Data
public class Product extends BaseEntity {
    private static final long serialVersionUID = 4489397646584896516L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String code;
    private String name;
    private String nameKh;
    private String imageUrl;
    private String stockType;
    private int qty;
    private Double discount;
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;
    private String status;
    private String tax;

}