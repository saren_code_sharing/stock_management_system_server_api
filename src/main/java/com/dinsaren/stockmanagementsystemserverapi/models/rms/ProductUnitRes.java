package com.dinsaren.stockmanagementsystemserverapi.models.rms;

import com.dinsaren.stockmanagementsystemserverapi.models.BaseEntity;
import com.dinsaren.stockmanagementsystemserverapi.models.Product;
import com.dinsaren.stockmanagementsystemserverapi.models.ProductUnit;
import com.dinsaren.stockmanagementsystemserverapi.models.UnitType;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
@Data
public class ProductUnitRes extends BaseEntity {
    private Integer id;
    private Integer unitTypeId;
    private Product product;
    private Double price;
    private Double cost;
    private String status;
    private String saleDefault;
    private UnitType unitType;
    private int quantity;
    private String imageUrl;

    public void setData(ProductUnit data){
        this.id=data.getId();
        this.unitType = data.getUnitType();
        this.product = data.getProduct();
        this.price=data.getPrice();
        this.cost=data.getCost();
        this.status=data.getStatus();
        this.saleDefault=data.getSaleDefault();
        this.setCreateAt(data.getCreateAt());
        this.setCreateBy(data.getCreateBy());
        this.setUpdateAt(data.getUpdateAt());
        this.setUpdateBy(data.getUpdateBy());
    }
}