package com.dinsaren.stockmanagementsystemserverapi.models.rms;

import lombok.Data;

import java.util.List;

@Data
public class CategoryRes {
    private Integer id;
    private String name;
    private String nameKh;
    private String imageUrl;
    private String status;
    private List<ProductRes> product;
}
