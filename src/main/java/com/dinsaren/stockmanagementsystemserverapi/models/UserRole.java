package com.dinsaren.stockmanagementsystemserverapi.models;

public enum UserRole {
  ROLE_CUSTOMER,
  ROLE_USER,
  ROLE_MODERATOR,
  ROLE_ADMIN,
  ROLE_SHOP,
}
