package com.dinsaren.stockmanagementsystemserverapi.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "sms_product_unit")
@DynamicUpdate()
@Data
public class ProductUnit extends BaseEntity {
    private static final long serialVersionUID = 4489397646584896516L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "unit_type_id")
    private UnitType unitType;
    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;
    private Double price;
    private Double cost;
    private Double  qty;
    private String status;
    private String saleDefault;

}