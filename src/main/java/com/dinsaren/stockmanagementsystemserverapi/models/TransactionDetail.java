package com.dinsaren.stockmanagementsystemserverapi.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "sms_transaction_detail")
@DynamicUpdate()
@Data
public class TransactionDetail extends BaseEntity {
    private static final long serialVersionUID = 4489397646584896516L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    @JoinColumn(name = "unit_type_id")
    private UnitType unitType;
    private int transactionId;
    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;
    private Integer qty;
    private Double price;
    private Double cost;
    private Double discount;
    private String status;

}