package com.dinsaren.stockmanagementsystemserverapi.models.res;

import com.dinsaren.stockmanagementsystemserverapi.models.BaseEntity;
import com.dinsaren.stockmanagementsystemserverapi.models.Product;
import com.dinsaren.stockmanagementsystemserverapi.models.ProductUnit;
import com.dinsaren.stockmanagementsystemserverapi.models.UnitType;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
@Data
public class ProductUnitRes extends BaseEntity {
    private Integer id;
    private Double price;
    private Double cost;
    private String status;
    private String saleDefault;
    private UnitType unitType;
    private Product product;

    public void setData(ProductUnit data) {
        this.id = data.getId();
        this.product = data.getProduct();
        this.unitType = data.getUnitType();
        this.price = data.getPrice();
        this.cost = data.getCost();
        this.status = data.getStatus();
        this.saleDefault = data.getSaleDefault();
        this.setCreateAt(data.getCreateAt());
        this.setCreateBy(data.getCreateBy());
        this.setUpdateAt(data.getUpdateAt());
        this.setUpdateBy(data.getUpdateBy());
    }
}