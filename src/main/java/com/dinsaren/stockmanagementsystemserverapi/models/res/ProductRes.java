package com.dinsaren.stockmanagementsystemserverapi.models.res;

import com.dinsaren.stockmanagementsystemserverapi.models.BaseEntity;
import com.dinsaren.stockmanagementsystemserverapi.models.Category;
import com.dinsaren.stockmanagementsystemserverapi.models.Product;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
@Data
public class ProductRes extends BaseEntity {
    private Integer id;
    private String code;
    private String name;
    private String nameKh;
    private String imageUrl;
    private String stockType;
    private int qty;
    private Double price;
    private Double cost;
    private Double discount;
    private int categoryId;
    private String status;
    private Category category;

    public void setData(Product data) {
        this.id = data.getId();
        this.name = data.getName();
        this.nameKh = data.getNameKh();
        this.code = data.getCode();
        this.imageUrl = data.getImageUrl();
        this.stockType = data.getStockType();
        this.qty = data.getQty();
        this.discount = data.getDiscount();
        this.category = data.getCategory();
        this.status = data.getStatus();
        this.setCreateAt(data.getCreateAt());
        this.setCreateBy(data.getCreateBy());
        this.setUpdateAt(data.getUpdateAt());
        this.setUpdateBy(data.getUpdateBy());
    }
}