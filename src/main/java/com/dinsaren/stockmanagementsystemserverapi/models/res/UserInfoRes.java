package com.dinsaren.stockmanagementsystemserverapi.models.res;

import com.dinsaren.stockmanagementsystemserverapi.models.User;
import lombok.Data;

import java.util.List;

@Data
public class UserInfoRes {
    private User user;
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
