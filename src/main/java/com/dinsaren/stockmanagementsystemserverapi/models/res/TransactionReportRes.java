package com.dinsaren.stockmanagementsystemserverapi.models.res;

import lombok.Data;

import java.util.List;

@Data
public class TransactionReportRes {
    private int totalItems;
    private Double totalAmount;
    private Double totalReceivedKHR;
    private Double totalReceivedUsd;
    private Double totalReturnKHR;
    private Double totalReturnUsd;
    private Double totalDiscount;
    public List<TransactionRes> transactionResList;
}
