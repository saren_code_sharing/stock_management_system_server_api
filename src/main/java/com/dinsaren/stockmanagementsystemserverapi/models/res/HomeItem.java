package com.dinsaren.stockmanagementsystemserverapi.models.res;

import lombok.Data;

@Data
public class HomeItem {
    private String name;
    private String nameKh;
    private String value;
    private String key;
    private int index;
}
