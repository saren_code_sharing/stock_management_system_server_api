package com.dinsaren.stockmanagementsystemserverapi.models.res;

import com.dinsaren.stockmanagementsystemserverapi.models.Bank;
import com.dinsaren.stockmanagementsystemserverapi.models.BaseEntity;
import com.dinsaren.stockmanagementsystemserverapi.models.Transaction;
import lombok.Data;

import java.util.List;

@Data
public class TransactionRes extends BaseEntity {
    private Integer id;
    private double totalItems;
    private Double totalAmount;
    private Double totalReceived;
    private Double totalReturn;
    private Double totalDiscount;
    private Double discount;
    private String transactionType;
    private Integer mainTransactionId;
    private Bank bank;
    private String status;
    private String transactionSession;
    private Double amountCcy;
    private String ccy;
    public List<TransactionItemRes> items;

    public void setData(Transaction data) {
        this.id = data.getId();
        this.totalItems = data.getTotalItems();
        this.totalAmount = data.getTotalAmount();
        this.discount = data.getDiscount();
        this.transactionType = data.getTransactionType();
        this.bank = data.getBank();
        this.status = data.getStatus();
        this.transactionSession = data.getTransactionSession();
        this.setCreateAt(data.getCreateAt());
        this.setCreateBy(data.getCreateBy());
        this.setUpdateAt(data.getUpdateAt());
        this.setUpdateBy(data.getUpdateBy());
    }

}
