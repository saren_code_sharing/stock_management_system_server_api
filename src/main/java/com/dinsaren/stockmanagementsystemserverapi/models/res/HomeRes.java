package com.dinsaren.stockmanagementsystemserverapi.models.res;

import lombok.Data;

import java.util.List;

@Data
public class HomeRes {
    private List<HomeItem> homeItem;
    private List<ProductRes> productDiscountList;
    private List<ProductUnitRes> productStockAlter;
}
