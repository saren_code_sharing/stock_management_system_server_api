package com.dinsaren.stockmanagementsystemserverapi.models.res;

import com.dinsaren.stockmanagementsystemserverapi.models.BaseEntity;
import com.dinsaren.stockmanagementsystemserverapi.models.Product;
import com.dinsaren.stockmanagementsystemserverapi.models.TransactionDetail;
import com.dinsaren.stockmanagementsystemserverapi.models.UnitType;
import lombok.Data;

@Data
public class TransactionItemRes extends BaseEntity {
    private int id;
    private int productId;
    private int unitTypeId;
    private int transactionId;
    private int qty;
    private Double price;
    private Double cost;
    private Double discount;
    private String status;
    private Product product;
    private UnitType unitType;
    private String imageUrl;

    public void setData(TransactionDetail data) {
        this.id = data.getId();
        this.unitType = data.getUnitType();
        this.transactionId = data.getTransactionId();
        this.qty = data.getQty();
        this.price = data.getPrice();
        this.cost = data.getCost();
        this.status = data.getStatus();
        this.discount = data.getDiscount();
        this.setCreateAt(data.getCreateAt());
        this.setCreateBy(data.getCreateBy());
        this.setUpdateAt(data.getUpdateAt());
        this.setUpdateBy(data.getUpdateBy());
    }

}
