package com.dinsaren.stockmanagementsystemserverapi.controllers.rest;

import com.dinsaren.stockmanagementsystemserverapi.constants.Constants;
import com.dinsaren.stockmanagementsystemserverapi.models.ExchangeRate;
import com.dinsaren.stockmanagementsystemserverapi.payload.response.MessageRes;
import com.dinsaren.stockmanagementsystemserverapi.repository.ExchangeRateRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/app/exchangeRate")
@Slf4j
@PreAuthorize("hasRole('USER') or hasRole('CUSTOMER') or hasRole('ADMIN')")
public class ExchangeRateController {
    private final ExchangeRateRepository exchangeRateRepository;
    private MessageRes messageRes;

    public ExchangeRateController(ExchangeRateRepository exchangeRateRepository) {
        this.exchangeRateRepository = exchangeRateRepository;
    }

    @GetMapping("/list")
    public ResponseEntity<MessageRes> getList() {
        messageRes = new MessageRes();
        List<ExchangeRate> list = new ArrayList<>();
        try {
            log.info("Intercept RMS get all Exchange Rate");
            list = exchangeRateRepository.findAllByStatusOrderByIdDesc(Constants.ACTIVE_STATUS);
            messageRes.setMessageSuccess(list);
        } catch (Throwable e) {
            log.info("While error RMS get all Exchange Rate ", e);
        } finally {
            log.info("Final RMS get info Exchange Rate response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MessageRes> getList(@PathVariable("id") Integer id) {
        messageRes = new MessageRes();
        try {
            log.info("Intercept RMS get all Exchange Rate");
            Optional<ExchangeRate> object = exchangeRateRepository.findByIdAndStatus(id, Constants.ACTIVE_STATUS);
            object.ifPresent(value -> messageRes.setMessageSuccess(value));
        } catch (Throwable e) {
            log.info("While error RMS get all Exchange Rate ", e);
        } finally {
            log.info("Final RMS get info Exchange Rate response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @GetMapping("/default")
    public ResponseEntity<MessageRes> getDefault() {
        messageRes = new MessageRes();
        try {
            log.info("Intercept RMS get  Exchange Rate ");
            Optional<ExchangeRate> object = exchangeRateRepository.findTopByOrderByIdDesc();
            object.ifPresent(value -> messageRes.setMessageSuccess(value));
        } catch (Throwable e) {
            log.info("While error RMS default ", e);
        } finally {
            log.info("Final RMS get info Exchange Rate response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<MessageRes> create(@RequestBody ExchangeRate req) {
        messageRes = new MessageRes();
        try {
            log.info("Intercept RMS create Exchange Rate {}", req);
            req.setStatus(Constants.ACTIVE_STATUS);
            req.setId(0);
            req.setCreateAt(new Date());
            req.setCreateBy(Constants.SYSTEM);
            req.setUpdateAt(new Date());
            req.setUpdateBy(Constants.SYSTEM);
            exchangeRateRepository.save(req);
            messageRes.setMessageSuccess("Create Success");
        } catch (Throwable e) {
            log.info("While error RMS create Exchange Rate ", e);
        } finally {
            log.info("Final RMS create Exchange Rate response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<MessageRes> update(@RequestBody ExchangeRate req) {
        messageRes = new MessageRes();
        try {
            Optional<ExchangeRate> find = exchangeRateRepository.findById(req.getId());
            if (find.isPresent()) {
                if (checkRequestBody(req)) return new ResponseEntity<>(messageRes, HttpStatus.BAD_REQUEST);
                req.setStatus(Constants.ACTIVE_STATUS);
                req.setCreateAt(new Date());
                req.setCreateBy(Constants.SYSTEM);
                req.setUpdateAt(new Date());
                req.setUpdateBy(Constants.SYSTEM);
                exchangeRateRepository.save(req);
            }

            messageRes.setMessageSuccess("update Success");
        } catch (Throwable e) {
            log.info("While error RMS update ExchangeRate ", e);
        } finally {
            log.info("Final RMS update ExchangeRate response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/delete")
    public ResponseEntity<MessageRes> delete(@RequestBody ExchangeRate req) {
        messageRes = new MessageRes();
        try {
            if (checkRequestBody(req)) return new ResponseEntity<>(messageRes, HttpStatus.BAD_REQUEST);
            log.info("Intercept RMS delete bank {}", req);
            req.setStatus(Constants.DELETE_STATUS);
            req.setCreateAt(new Date());
            req.setCreateBy(Constants.SYSTEM);
            req.setUpdateAt(new Date());
            req.setUpdateBy(Constants.SYSTEM);
            exchangeRateRepository.save(req);
            messageRes.setMessageSuccess("delete Success");
        } catch (Throwable e) {
            log.info("While error RMS delete ExchangeRate ", e);
        } finally {
            log.info("Final RMS delete ExchangeRate response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    private boolean checkRequestBody(@RequestBody ExchangeRate req) {
        Optional<ExchangeRate> bank = exchangeRateRepository.findByIdAndStatus(req.getId(), Constants.ACTIVE_STATUS);
        if (!bank.isPresent()) {
            messageRes.badRequest("Error: Data not found!");
            return true;
        }
        return false;
    }


}
