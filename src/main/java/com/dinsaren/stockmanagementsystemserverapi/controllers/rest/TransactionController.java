package com.dinsaren.stockmanagementsystemserverapi.controllers.rest;

import com.dinsaren.stockmanagementsystemserverapi.constants.Constants;
import com.dinsaren.stockmanagementsystemserverapi.models.*;
import com.dinsaren.stockmanagementsystemserverapi.models.req.GetTransactionReq;
import com.dinsaren.stockmanagementsystemserverapi.models.req.TransactionItemReq;
import com.dinsaren.stockmanagementsystemserverapi.models.req.TransactionReq;
import com.dinsaren.stockmanagementsystemserverapi.models.res.TransactionItemRes;
import com.dinsaren.stockmanagementsystemserverapi.models.res.TransactionReportRes;
import com.dinsaren.stockmanagementsystemserverapi.models.res.TransactionRes;
import com.dinsaren.stockmanagementsystemserverapi.payload.response.MessageRes;
import com.dinsaren.stockmanagementsystemserverapi.repository.*;
import com.dinsaren.stockmanagementsystemserverapi.services.AuthenticationUtilService;
import com.dinsaren.stockmanagementsystemserverapi.utils.FormatDateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/app/transaction")
@Slf4j
@PreAuthorize("hasRole('USER') or hasRole('CUSTOMER') or hasRole('ADMIN')")
public class TransactionController {
    private final TransactionRepository transactionRepository;
    private final TransactionDetailRepository transactionDetailRepository;
    private final ProductRepository productRepository;
    private final ProductUnitTypeRepository productUnitTypeRepository;
    private final UnitTypeRepository unitTypeRepository;
    private final BankRepository bankRepository;
    private MessageRes messageRes;
    private User user;
    private final AuthenticationUtilService authenticationUtilService;
    public TransactionController(TransactionRepository transactionRepository, TransactionDetailRepository transactionDetailRepository, ProductRepository productRepository, ProductUnitTypeRepository productUnitTypeRepository, UnitTypeRepository unitTypeRepository, BankRepository bankRepository, AuthenticationUtilService authenticationUtilService) {
        this.transactionRepository = transactionRepository;
        this.transactionDetailRepository = transactionDetailRepository;
        this.productRepository = productRepository;
        this.productUnitTypeRepository = productUnitTypeRepository;
        this.unitTypeRepository = unitTypeRepository;
        this.bankRepository = bankRepository;
        this.authenticationUtilService = authenticationUtilService;
    }

    @PostMapping("/update")
    @Transactional
    public ResponseEntity<MessageRes> update(@RequestBody TransactionReq req) {
        messageRes = new MessageRes();
        try {
            log.info("Intercept RMS create transaction {}", req);
            Optional<Transaction> findTransaction = transactionRepository.findByIdAndStatus(req.getId(), Constants.TXN_INIT);
            if (findTransaction.isPresent()) {
                Transaction transaction = findTransaction.get();
                transaction.setStatus(Constants.ACTIVE_STATUS);
                transaction.setCreateAt(new Date());
                transaction.setCreateBy(Constants.SYSTEM);
                transaction.setUpdateBy(Constants.SYSTEM);
                transaction.setStatus(req.getStatus());
                transaction.setTotalAmount(req.getTotalAmount());
                transaction.setDiscount(req.getDiscount());
                transaction.setDiscount(req.getDiscount());
                Bank bank = bankRepository.findById(req.getBankId()).orElse(null);
                transaction.setBank(bank);
                transaction.setTotalItems(req.getTotalItems());
                transaction.setTransactionSession(UUID.randomUUID().toString());
                transaction.setTransactionType(req.getTransactionType());
                transaction.setStatus(req.getStatus());
                Transaction createTransaction = transactionRepository.save(transaction);
                if (createTransaction.getId() > 0) {
                    for (TransactionItemReq itemReq : req.items) {
                        Optional<TransactionDetail> findTranscationDetail = transactionDetailRepository.findByIdAndStatus(itemReq.getId(), Constants.ACTIVE_STATUS);
                        if (findTranscationDetail.isPresent()) {
                            TransactionDetail detail = findTranscationDetail.get();
                            detail.setDiscount(itemReq.getDiscount());
                            detail.setCreateAt(new Date());
                            detail.setCreateBy(Constants.SYSTEM);
                            detail.setUpdateBy(Constants.SYSTEM);
                            detail.setQty(itemReq.getQty());
                            Product  product = productRepository.findById(itemReq.getProductId()).orElse(null);
                            detail.setProduct(product);
                            detail.setTransactionId(createTransaction.getId());
                            detail.setStatus(Constants.ACTIVE_STATUS);
                            UnitType unitType = unitTypeRepository.findById(itemReq.getUnitTypeId()).orElse(null);
                            detail.setUnitType(unitType);
                            detail.setCost(itemReq.getCost());
                            detail.setPrice(itemReq.getPrice());
                            transactionDetailRepository.save(detail);
                        }
                    }
                }
            }
            messageRes.setMessageSuccess("create Success");
        } catch (Throwable e) {
            log.info("While error RMS create transaction ", e);
        } finally {
            log.info("Final RMS create transaction response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/create")
    @Transactional
    public ResponseEntity<MessageRes> create(@RequestBody TransactionReq req) {
        messageRes = new MessageRes();
        try {
            log.info("Intercept RMS update transaction {}", req);
            user = authenticationUtilService.checkUser();
            log.info("Intercept RMS item restaurant transaction {}", req);
            if (null == user) {
                return new ResponseEntity<>(messageRes, HttpStatus.BAD_GATEWAY);
            }
            Transaction transaction = new Transaction();
            transaction.setCreateAt(new Date());
            transaction.setCreateBy(Constants.SYSTEM);
            transaction.setUpdateBy(Constants.SYSTEM);
            transaction.setStatus(req.getStatus());
            transaction.setTotalAmount(req.getTotalAmount());
            transaction.setDiscount(req.getDiscount());
            transaction.setDiscount(req.getDiscount());
            Bank bank = bankRepository.findById(req.getBankId()).orElse(null);
            transaction.setBank(bank);
            transaction.setTransactionSession(UUID.randomUUID().toString());
            transaction.setTransactionType(req.getTransactionType());
            transaction.setStatus(req.getStatus());
            Transaction createTransaction = transactionRepository.save(transaction);
            if (createTransaction.getId() > 0) {
                for (TransactionItemReq itemReq : req.items) {
                    TransactionDetail detail = new TransactionDetail();
                    detail.setDiscount(itemReq.getDiscount());
                    detail.setCreateAt(new Date());
                    detail.setCreateBy(Constants.SYSTEM);
                    detail.setUpdateBy(Constants.SYSTEM);
                    detail.setQty(itemReq.getQty());
                    Product  product = productRepository.findById(itemReq.getProductId()).orElse(null);
                    detail.setProduct(product);
                    detail.setTransactionId(createTransaction.getId());
                    detail.setStatus(Constants.ACTIVE_STATUS);
                    UnitType unitType = unitTypeRepository.findById(itemReq.getUnitTypeId()).orElse(null);
                    detail.setUnitType(unitType);
                    detail.setCost(itemReq.getCost());
                    detail.setPrice(itemReq.getPrice());
                    transactionDetailRepository.save(detail);
                }
                messageRes.setMessageSuccess(createTransaction.getTransactionSession());
            }

        } catch (Throwable e) {
            log.info("While error RMS update transaction ", e);
        } finally {
            log.info("Final RMS update transaction response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/list")
    @Transactional
    public ResponseEntity<MessageRes> getTransactionList(@RequestBody GetTransactionReq req) {
        messageRes = new MessageRes();
        try {
            log.info("Intercept RMS get transaction list {}", req);
            List<TransactionRes> list = new ArrayList<>();
            List<TransactionItemRes> itemResList = new ArrayList<>();
            transactionRepository.findAllByStatusAndTransactionType(req.getStatus(), req.getTransactionType()).forEach(transaction -> {
                        TransactionRes res = new TransactionRes();
                        res.setData(transaction);
                        res.setBank(transaction.getBank());
                        List<TransactionDetail> transactionDetailList = transactionDetailRepository.findAllByTransactionId(transaction.getId());
                        if (!transactionDetailList.isEmpty()) {
                            for (TransactionDetail detail : transactionDetailList) {
                                TransactionItemRes itemRes = new TransactionItemRes();
                                itemRes.setData(detail);
                                itemRes.setProduct(detail.getProduct());
                                itemRes.setUnitType(detail.getUnitType());
                                itemResList.add(itemRes);
                            }
                        }
                        res.setItems(itemResList);
                        list.add(res);
                    });
            messageRes.setMessageSuccess(list);
        } catch (Throwable e) {
            log.info("While error RMS create transaction ", e);
        } finally {
            log.info("Final RMS create transaction response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/v1/list")
    @Transactional
    public ResponseEntity<MessageRes> getTransactionListByPaginationV1(@RequestBody GetTransactionReq req) {
        messageRes = new MessageRes();
        try {
            log.info("Intercept RMS get transaction list {}", req);
            req.setLimit(10);
            Pageable paging = PageRequest.of(req.getPage(), req.getLimit());
            List<TransactionRes> list = new ArrayList<>();
            List<TransactionItemRes> itemResList = new ArrayList<>();
            transactionRepository.findAllByStatusAndTransactionTypeOrderByIdDesc(req.getStatus(), req.getTransactionType(), paging).getContent().forEach(transaction -> {
                TransactionRes res = new TransactionRes();
                res.setData(transaction);
                res.setBank(transaction.getBank());
                list.add(res);
            });
            messageRes.setMessageSuccess(list);
        } catch (Throwable e) {
            log.info("While error RMS create transaction ", e);
        } finally {
            log.info("Final RMS create transaction response ");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/find")
    @Transactional
    public ResponseEntity<MessageRes> findTransaction(@RequestBody GetTransactionReq req) {
        messageRes = new MessageRes();
        try {
            log.info("Intercept RMS get transaction list {}", req);
            TransactionRes res = new TransactionRes();
            List<TransactionItemRes> itemResList = new ArrayList<>();

            Optional<Transaction> transaction = transactionRepository.findByTransactionSessionAndStatus(req.getTransactionSession(), req.getStatus());
            if (transaction.isPresent()) {
                res.setData(transaction.get());
                res.setBank(transaction.get().getBank());
                for (TransactionDetail detail : transactionDetailRepository.findAllByTransactionId(transaction.get().getId())) {
                    TransactionItemRes itemRes = new TransactionItemRes();
                    itemRes.setData(detail);
                    itemRes.setProduct(detail.getProduct());
                    itemRes.setUnitType(detail.getUnitType());
                    itemResList.add(itemRes);
                }
                res.setItems(itemResList);

            }
            messageRes.setMessageSuccess(res);
        } catch (Throwable e) {
            log.info("While error RMS create transaction ", e);
        } finally {
            log.info("Final RMS create transaction response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @GetMapping("/report")
    public ResponseEntity<MessageRes> getByCreatedDateEndCreatedDate(@RequestParam("start_date") String startDate, @RequestParam("end_date") String endDate, @RequestParam("type") String type) {
        messageRes = new MessageRes();
        TransactionReportRes transactionReportRes = new TransactionReportRes();
        try {
            log.info("Transaction start date {} and end date {} and type {}", startDate, endDate, type);
            if (type.equals("")) {
                Date start = FormatDateUtils.convertStringToDateYyyyDdMm(startDate);
                Date end = FormatDateUtils.convertStringToDateYyyyDdMm(endDate);
                List<Transaction> transactionList = transactionRepository.findAllByCreateAtGreaterThanEqualAndCreateAtLessThanEqualOrderByIdDesc(start, end);
                List<TransactionRes> transactionResList = new ArrayList<>();
                List<TransactionItemRes> itemResList = new ArrayList<>();
                int totalItems = 0;
                double totalAmount = 0.0;
                double totalReceivedKHR = 0.0;
                double totalReceivedUsd = 0.0;
                double totalReturnKHR = 0.0;
                double totalReturnUsd = 0.0;
                double totalDiscount = 0.0;
                GeneratorTransactionRes(totalItems, totalAmount, totalReceivedKHR, totalReceivedUsd, totalReturnKHR, totalReturnUsd, totalDiscount, transactionReportRes, transactionResList, itemResList, transactionList, bankRepository, transactionDetailRepository, productRepository, unitTypeRepository, productUnitTypeRepository);
            } else {
                transactionReportRes = getByCreatedDateEndCreatedDate(type);
            }
            messageRes.setMessageSuccess(transactionReportRes);
        } catch (Throwable e) {
            log.info("While error RMS create transaction ", e);
        } finally {
            log.info("Final RMS create transaction response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    public TransactionReportRes getByCreatedDateEndCreatedDate(String type) {
        int totalItems = 0;
        double totalAmount = 0.0;
        double totalReceivedKHR = 0.0;
        double totalReceivedUsd = 0.0;
        double totalReturnKHR = 0.0;
        double totalReturnUsd = 0.0;
        double totalDiscount = 0.0;
        Calendar c = GregorianCalendar.getInstance();
        Date startDate =new Date();
        Date endDate = new Date();
        if(Constants.TODAY.equals(type)){
            startDate = FormatDateUtils.convertStringToDateYyyyDdMm(LocalDate.now().format(DateTimeFormatter.ofPattern(FormatDateUtils.yyyy_dd_MM)));
            endDate = FormatDateUtils.convertStringToDateYyyyDdMm(LocalDate.now().format(DateTimeFormatter.ofPattern(FormatDateUtils.yyyy_dd_MM)));
        }else if(Constants.THIS_WEEK.equals(type)) {
            // Get calendar set to current date and time
            c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            DateFormat df = new SimpleDateFormat(FormatDateUtils.yyyy_dd_MM, Locale.getDefault());
            startDate = FormatDateUtils.convertStringToDateYyyyDdMm(df.format(c.getTime()));
            endDate = FormatDateUtils.convertStringToDateYyyyDdMm(LocalDate.now().format(DateTimeFormatter.ofPattern(FormatDateUtils.yyyy_dd_MM)));
        }else if(Constants.THIS_MONTH.equals(type)) {
            c.set(Calendar.DAY_OF_WEEK, Calendar.MONTH);
            DateFormat df = new SimpleDateFormat(FormatDateUtils.yyyy_dd_MM, Locale.getDefault());
            startDate = FormatDateUtils.convertStringToDateYyyyDdMm(df.format(c.getTime()));
            endDate = FormatDateUtils.convertStringToDateYyyyDdMm(LocalDate.now().format(DateTimeFormatter.ofPattern(FormatDateUtils.yyyy_dd_MM)));
        }else if (Constants.THIS_YEAR.equals(type)) {
            c.set(Calendar.DAY_OF_YEAR, Calendar.YEAR);
            DateFormat df = new SimpleDateFormat(FormatDateUtils.yyyy_dd_MM, Locale.getDefault());
            startDate = FormatDateUtils.convertStringToDateYyyyDdMm(df.format(c.getTime()));
            endDate = FormatDateUtils.convertStringToDateYyyyDdMm(LocalDate.now().format(DateTimeFormatter.ofPattern(FormatDateUtils.yyyy_dd_MM)));
        }else{
            startDate = FormatDateUtils.convertStringToDateYyyyDdMm(LocalDate.now().format(DateTimeFormatter.ofPattern(FormatDateUtils.yyyy_dd_MM)));
            endDate = FormatDateUtils.convertStringToDateYyyyDdMm(LocalDate.now().format(DateTimeFormatter.ofPattern(FormatDateUtils.yyyy_dd_MM)));
        }

        TransactionReportRes transactionReportRes = new TransactionReportRes();
        List<TransactionRes> transactionResList = new ArrayList<>();
        List<TransactionItemRes> itemResList = new ArrayList<>();
        List<Transaction> transactionList = transactionRepository.findAllByCreateAtBetweenOrderById(startDate, endDate);
        GeneratorTransactionRes(totalItems, totalAmount, totalReceivedKHR, totalReceivedUsd, totalReturnKHR, totalReturnUsd, totalDiscount, transactionReportRes, transactionResList, itemResList, transactionList, bankRepository, transactionDetailRepository, productRepository, unitTypeRepository, productUnitTypeRepository);
        return transactionReportRes;
    }

    private static void GeneratorTransactionRes(int totalItems, double totalAmount, double totalReceivedKHR, double totalReceivedUsd, double totalReturnKHR, double totalReturnUsd, double totalDiscount, TransactionReportRes transactionReportRes, List<TransactionRes> transactionResList, List<TransactionItemRes> itemResList, List<Transaction> transactionList, BankRepository bankRepository, TransactionDetailRepository transactionDetailRepository, ProductRepository productRepository, UnitTypeRepository unitTypeRepository, ProductUnitTypeRepository productUnitTypeRepository) {
        for (Transaction transaction : transactionList) {
            TransactionRes res = new TransactionRes();
            res.setData(transaction);
            res.setBank(transaction.getBank());
            totalItems += transaction.getTotalItems();
            res.setItems(itemResList);
            transactionResList.add(res);
        }
        transactionReportRes.setTotalItems(totalItems);
        transactionReportRes.setTotalReceivedUsd(totalReceivedUsd);
        transactionReportRes.setTotalReceivedKHR(totalReceivedKHR);
        transactionReportRes.setTotalDiscount(totalDiscount);
        transactionReportRes.setTotalReturnKHR(totalReturnKHR);
        transactionReportRes.setTotalReturnUsd(totalReturnUsd);
        transactionReportRes.setTotalAmount(totalAmount);
        transactionReportRes.setTransactionResList(transactionResList);
    }


}
