package com.dinsaren.stockmanagementsystemserverapi.controllers.rest;

import com.dinsaren.stockmanagementsystemserverapi.constants.Constants;
import com.dinsaren.stockmanagementsystemserverapi.models.ProductUnit;
import com.dinsaren.stockmanagementsystemserverapi.models.UnitType;
import com.dinsaren.stockmanagementsystemserverapi.models.res.ProductUnitRes;
import com.dinsaren.stockmanagementsystemserverapi.payload.response.MessageRes;
import com.dinsaren.stockmanagementsystemserverapi.repository.CategoryRepository;
import com.dinsaren.stockmanagementsystemserverapi.repository.ProductRepository;
import com.dinsaren.stockmanagementsystemserverapi.repository.ProductUnitTypeRepository;
import com.dinsaren.stockmanagementsystemserverapi.repository.UnitTypeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/app/productUnit")
@Slf4j
@PreAuthorize("hasRole('USER') or hasRole('CUSTOMER') or hasRole('ADMIN')")
public class ProductUnitController {
    private final ProductRepository productRepository;
    private final ProductUnitTypeRepository productUnitTypeRepository;
    private final UnitTypeRepository unitTypeRepository;
    private final CategoryRepository categoryRepository;
    private MessageRes messageRes;

    public ProductUnitController(ProductRepository productRepository, ProductUnitTypeRepository productUnitTypeRepository, UnitTypeRepository unitTypeRepository, CategoryRepository categoryRepository) {
        this.productRepository = productRepository;
        this.productUnitTypeRepository = productUnitTypeRepository;
        this.unitTypeRepository = unitTypeRepository;
        this.categoryRepository = categoryRepository;
    }

    @GetMapping("/list/{productId}")
    public ResponseEntity<MessageRes> getList(@PathVariable("productId") Integer productId) {
        messageRes = new MessageRes();
        List<ProductUnitRes> list = new ArrayList<>();
        try {
            log.info("Intercept RMS get all product unit");
            productUnitTypeRepository.findAllByProductIdAndStatus(productId, Constants.ACTIVE_STATUS).forEach(p -> {
                ProductUnitRes res = new ProductUnitRes();
                res.setData(p);
                res.setUnitType(p.getUnitType());
                list.add(res);
            });
            messageRes.setMessageSuccess(list);
        } catch (Throwable e) {
            log.info("While error RMS get all product unit", e);
        } finally {
            log.info("Final RMS get info response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MessageRes> getById(@PathVariable("id") Integer id) {
        messageRes = new MessageRes();
        ProductUnitRes res = new ProductUnitRes();
        try {
            log.info("Intercept RMS get all product unit");
            Optional<ProductUnit> object = productUnitTypeRepository.findByIdAndStatus(id, Constants.ACTIVE_STATUS);
            if (object.isPresent()) {
                res.setData(object.get());
                res.setUnitType(object.get().getUnitType());
                messageRes.setMessageSuccess(res);
            }else{
                messageRes.setMessageSuccess(null);
            }

        } catch (Throwable e) {
            log.info("While error RMS get all product unit", e);
        } finally {
            log.info("Final RMS get info response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<MessageRes> create(@RequestBody ProductUnit req) {
        messageRes = new MessageRes();
        try {
            Optional<ProductUnit> productUnit = productUnitTypeRepository.findByProductIdAndUnitTypeId(req.getProduct().getId(),req.getUnitType().getId());
            if(productUnit.isPresent()){
                messageRes.setMessageSuccess("Duplicate unit type Success");
            }else{
                log.info("Intercept RMS create product unit {}", req);
                req.setStatus(Constants.ACTIVE_STATUS);
                req.setId(0);
                req.setCreateAt(new Date());
                req.setCreateBy(Constants.SYSTEM);
                req.setUpdateAt(new Date());
                req.setUpdateBy(Constants.SYSTEM);
                productUnitTypeRepository.save(req);
                messageRes.setMessageSuccess("Create Success");
            }

        } catch (Throwable e) {
            log.info("While error RMS create product unit", e);
        } finally {
            log.info("Final RMS create bank response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<MessageRes> update(@RequestBody ProductUnit req) {
        messageRes = new MessageRes();
        try {
            log.info("Intercept RMS update product unit {}", req);
            if (checkRequestBody(req)) return new ResponseEntity<>(messageRes, HttpStatus.BAD_REQUEST);
            Optional<ProductUnit> productUnit = productUnitTypeRepository.findByProductIdAndUnitTypeId(req.getProduct().getId(),req.getUnitType().getId());
            if(productUnit.isPresent()){
                messageRes.setMessageSuccess("Duplicate unit type Success");
            }else {
                req.setStatus(Constants.ACTIVE_STATUS);
                req.setCreateAt(new Date());
                req.setCreateBy(Constants.SYSTEM);
                req.setUpdateAt(new Date());
                req.setUpdateBy(Constants.SYSTEM);
                productUnitTypeRepository.saveAndFlush(req);
                messageRes.setMessageSuccess("update Success");
            }
        } catch (Throwable e) {
            log.info("While error RMS update product unit ", e);
        } finally {
            log.info("Final RMS update product response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/delete")
    public ResponseEntity<MessageRes> delete(@RequestBody ProductUnit req) {
        messageRes = new MessageRes();
        try {
            if (checkRequestBody(req)) return new ResponseEntity<>(messageRes, HttpStatus.BAD_REQUEST);
            log.info("Intercept RMS delete product unit {}", req);
            req.setStatus(Constants.ACTIVE_STATUS);
            productUnitTypeRepository.save(req);
            messageRes.setMessageSuccess("delete Success");
        } catch (Throwable e) {
            log.info("While error RMS delete product unit ", e);
        } finally {
            log.info("Final RMS delete bank response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    private boolean checkRequestBody(@RequestBody ProductUnit req) {
        if (req.getId() < 0) {
            messageRes.badRequest("Error: Invalid request!");
            return true;
        }
        Optional<ProductUnit> bank = productUnitTypeRepository.findByIdAndStatus(req.getId(), Constants.ACTIVE_STATUS);
        if (!bank.isPresent()) {
            messageRes.badRequest("Error: Data not found!");
            return true;
        }
        return false;
    }


}
