package com.dinsaren.stockmanagementsystemserverapi.controllers.rest;

import com.dinsaren.stockmanagementsystemserverapi.constants.Constants;
import com.dinsaren.stockmanagementsystemserverapi.models.Category;
import com.dinsaren.stockmanagementsystemserverapi.models.Product;
import com.dinsaren.stockmanagementsystemserverapi.models.ProductUnit;
import com.dinsaren.stockmanagementsystemserverapi.models.UnitType;
import com.dinsaren.stockmanagementsystemserverapi.models.res.ProductRes;
import com.dinsaren.stockmanagementsystemserverapi.models.res.ProductUnitRes;
import com.dinsaren.stockmanagementsystemserverapi.payload.response.MessageRes;
import com.dinsaren.stockmanagementsystemserverapi.repository.CategoryRepository;
import com.dinsaren.stockmanagementsystemserverapi.repository.ProductRepository;
import com.dinsaren.stockmanagementsystemserverapi.repository.ProductUnitTypeRepository;
import com.dinsaren.stockmanagementsystemserverapi.repository.UnitTypeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/app/product")
@Slf4j
@PreAuthorize("hasRole('USER') or hasRole('CUSTOMER') or hasRole('ADMIN')")
public class ProductController {
    private final ProductRepository productRepository;
    private final ProductUnitTypeRepository productUnitTypeRepository;
    private final UnitTypeRepository unitTypeRepository;
    private final CategoryRepository categoryRepository;
    private MessageRes messageRes;

    public ProductController(ProductRepository productRepository, ProductUnitTypeRepository productUnitTypeRepository, UnitTypeRepository unitTypeRepository, CategoryRepository categoryRepository) {
        this.productRepository = productRepository;
        this.productUnitTypeRepository = productUnitTypeRepository;
        this.unitTypeRepository = unitTypeRepository;
        this.categoryRepository = categoryRepository;
    }

    @GetMapping("/list")
    public ResponseEntity<MessageRes> getList() {
        messageRes = new MessageRes();
        List<ProductRes> list = new ArrayList<>();
        try {
            log.info("Intercept RMS get all product");
            productRepository.findAllByStatus(Constants.ACTIVE_STATUS).forEach(p -> {
                ProductRes res = new ProductRes();
                res.setData(p);
                res.setCategory(p.getCategory());
                list.add(res);
            });
            messageRes.setMessageSuccess(list);
        } catch (Throwable e) {
            log.info("While error RMS get all product ", e);
        } finally {
            log.info("Final RMS get info response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @GetMapping("/productUnit/list")
    public ResponseEntity<MessageRes> getProductUnitList() {
        messageRes = new MessageRes();
        List<ProductUnitRes> list = new ArrayList<>();
        try {
            log.info("Intercept RMS get all product unit");
            productUnitTypeRepository.findAllByStatusOrderByProductId(Constants.ACTIVE_STATUS).forEach(p -> {
                ProductUnitRes res = new ProductUnitRes();
                res.setData(p);
                res.setProduct(p.getProduct());
                res.setUnitType(p.getUnitType());
                list.add(res);
            });
            messageRes.setMessageSuccess(list);
        } catch (Throwable e) {
            log.info("While error RMS get all product unit", e);
        } finally {
            log.info("Final RMS get info response ");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MessageRes> getList(@PathVariable("id") Integer id) {
        messageRes = new MessageRes();
        ProductRes res = new ProductRes();
        try {
            log.info("Intercept get all product");
            Optional<Product> object = productRepository.findByIdAndStatus(id, Constants.ACTIVE_STATUS);
            if (object.isPresent()) {
                res.setData(object.get());
                res.setCategory(object.get().getCategory());
                messageRes.setMessageSuccess(res);
            }else{
                messageRes.dataNotFound("Product not found");
                return new ResponseEntity<>(messageRes, HttpStatus.OK);
            }
        } catch (Throwable e) {
            log.info("While error RMS get all product ", e);
        } finally {
            log.info("Final RMS get info response ");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<MessageRes> create(@RequestBody Product req) {
        messageRes = new MessageRes();
        try {
            log.info("Intercept RMS create product {}", req);
            req.setStatus(Constants.ACTIVE_STATUS);
            req.setId(0);
            req.setCreateAt(new Date());
            req.setCreateBy(Constants.SYSTEM);
            req.setUpdateAt(new Date());
            req.setUpdateBy(Constants.SYSTEM);
            productRepository.save(req);
            messageRes.setMessageSuccess("Create Success");
        } catch (Throwable e) {
            log.info("While error RMS create product ", e);
        } finally {
            log.info("Final RMS create bank response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<MessageRes> update(@RequestBody Product req) {
        messageRes = new MessageRes();
        try {

            if (checkRequestBody(req)) return new ResponseEntity<>(messageRes, HttpStatus.BAD_REQUEST);
            log.info("Intercept RMS update product {}", req);
            req.setStatus(Constants.ACTIVE_STATUS);
            req.setCreateAt(new Date());
            req.setCreateBy(Constants.SYSTEM);
            req.setUpdateAt(new Date());
            req.setUpdateBy(Constants.SYSTEM);
            productRepository.save(req);
            messageRes.setMessageSuccess("update Success");
        } catch (Throwable e) {
            log.info("While error RMS update product ", e);
        } finally {
            log.info("Final RMS update product response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/delete")
    public ResponseEntity<MessageRes> delete(@RequestBody Product req) {
        messageRes = new MessageRes();
        try {
            if (checkRequestBody(req)) return new ResponseEntity<>(messageRes, HttpStatus.BAD_REQUEST);
            log.info("Intercept RMS delete product {}", req);
            req.setStatus(Constants.DELETE_STATUS);
            req.setCreateAt(new Date());
            req.setCreateBy(Constants.SYSTEM);
            req.setUpdateAt(new Date());
            req.setUpdateBy(Constants.SYSTEM);
            productRepository.save(req);
            messageRes.setMessageSuccess("delete Success");
        } catch (Throwable e) {
            log.info("While error RMS delete product ", e);
        } finally {
            log.info("Final RMS delete bank response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    private boolean checkRequestBody(@RequestBody Product req) {
        if (req.getId() < 0 || req.getName().equals("") || null == req.getName()) {
            messageRes.badRequest("Error: Invalid request!");
            return true;
        }
        Optional<Product> bank = productRepository.findByIdAndStatus(req.getId(), Constants.ACTIVE_STATUS);
        if (!bank.isPresent()) {
            messageRes.badRequest("Error: Data not found!");
            return true;
        }
        return false;
    }


}
