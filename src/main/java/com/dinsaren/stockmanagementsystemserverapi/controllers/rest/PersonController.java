package com.dinsaren.stockmanagementsystemserverapi.controllers.rest;


import com.dinsaren.stockmanagementsystemserverapi.constants.Constants;
import com.dinsaren.stockmanagementsystemserverapi.models.Person;
import com.dinsaren.stockmanagementsystemserverapi.models.User;
import com.dinsaren.stockmanagementsystemserverapi.payload.response.MessageRes;
import com.dinsaren.stockmanagementsystemserverapi.repository.PersonRepository;
import com.dinsaren.stockmanagementsystemserverapi.services.AuthenticationUtilService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/app/person")
@Slf4j
@PreAuthorize("hasRole('USER') or hasRole('CUSTOMER') or hasRole('ADMIN')")
public class PersonController {
    private MessageRes messageRes;
    private final AuthenticationUtilService authenticationUtilService;
    private final PersonRepository userPersonRepository;
    private User user;

    public PersonController(AuthenticationUtilService authenticationUtilService, PersonRepository userPersonRepository) {
        this.authenticationUtilService = authenticationUtilService;
        this.userPersonRepository = userPersonRepository;
    }

    @GetMapping("/list/{type}")
    public ResponseEntity<MessageRes> getPersonBusinessIdAndType(@PathVariable("type") String type) {
        messageRes = new MessageRes();
        List<Person> userPersonList;
        try {
            user = authenticationUtilService.checkUser();
            log.info("Intercept user get all person ");
            if (null == user) {
                return new ResponseEntity<>(messageRes, HttpStatus.BAD_GATEWAY);
            }

            userPersonList = userPersonRepository.findAllByPersonTypeAndStatus(type, Constants.ACTIVE_STATUS);
            messageRes.setMessageSuccess(userPersonList);

        } catch (Throwable e) {
            log.info("While error user get all user person list", e);
        } finally {
            log.info("Final user get all user person list response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }
    @GetMapping("/default/{type}")
    public ResponseEntity<MessageRes> getDefault(@PathVariable("type") String type) {
        messageRes = new MessageRes();
        try {
            log.info("Intercept RMS get default ");
            Optional<Person> check = userPersonRepository.findByCheckDefaultAndPersonType(Constants.YES, type);
            check.ifPresent(value -> messageRes.setMessageSuccess(value));
        } catch (Throwable e) {
            log.info("While error RMS default ", e);
        } finally {
            log.info("Final RMS get info response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MessageRes> getById(@PathVariable("id") Integer id) {
        messageRes = new MessageRes();
        Person userPerson;
        try {
            user = authenticationUtilService.checkUser();
            log.info("Intercept user get user person by id {}", id);
            if (null == user) {
                return new ResponseEntity<>(messageRes, HttpStatus.BAD_GATEWAY);
            }
            userPerson = userPersonRepository.findByIdAndStatus(id, Constants.ACTIVE_STATUS);
            messageRes.setMessageSuccess(userPerson);

        } catch (Throwable e) {
            log.info("While error user get user person by id", e);
        } finally {
            log.info("Final user get user person list response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<MessageRes> create(@RequestBody Person req) {
        messageRes = new MessageRes();
        try {
            user = authenticationUtilService.checkUser();
            log.info("Intercept user create user person list req {}", req);
            if (null == user) {
                return new ResponseEntity<>(messageRes, HttpStatus.BAD_GATEWAY);
            }
            if(req.getCheckDefault().equals(Constants.YES)){
                Optional<Person> check = userPersonRepository.findByCheckDefaultAndPersonType(Constants.YES, req.getPersonType());
                if(check.isPresent()){
                    check.get().setCheckDefault(Constants.NO);
                    userPersonRepository.save(check.get());
                }

            }
            req.setStatus(Constants.ACTIVE_STATUS);
            req.setCreateAt(new Date());
            req.setCreateBy(Constants.SYSTEM);
            req.setUpdateAt(new Date());
            req.setUpdateBy(Constants.SYSTEM);
            userPersonRepository.save(req);
            messageRes.setMessageSuccess(null);

        } catch (Throwable e) {
            log.info("While error user get all user person list", e);
        } finally {
            log.info("Final user get all user person list response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<MessageRes> update(@RequestBody Person req) {
        messageRes = new MessageRes();
        try {
            user = authenticationUtilService.checkUser();
            log.info("Intercept user update user person list req {}", req);
            if (null == user) {
                return new ResponseEntity<>(messageRes, HttpStatus.BAD_GATEWAY);
            }

            Person findById = userPersonRepository.findByIdAndStatus(req.getId(), Constants.ACTIVE_STATUS);
            if (null == findById) {
                return new ResponseEntity<>(messageRes, HttpStatus.BAD_REQUEST);
            }
            if (req.getCheckDefault().equals(Constants.YES)) {
                Optional<Person> check = userPersonRepository.findByCheckDefaultAndPersonType(Constants.YES, req.getPersonType());
                if (check.isPresent()) {
                    check.get().setCheckDefault(Constants.NO);
                    userPersonRepository.save(check.get());
                }

            }
            req.setStatus(Constants.ACTIVE_STATUS);
            req.setCreateAt(new Date());
            req.setCreateBy(Constants.SYSTEM);
            req.setUpdateAt(new Date());
            req.setUpdateBy(Constants.SYSTEM);
            userPersonRepository.save(req);
            messageRes.setMessageSuccess(null);

        } catch (Throwable e) {
            log.info("While error user update user business", e);
        } finally {
            log.info("Final user update user business  response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/delete")
    public ResponseEntity<MessageRes> delete(@RequestBody Person req) {
        messageRes = new MessageRes();
        try {
            user = authenticationUtilService.checkUser();
            log.info("Intercept user update user person list req {}", req);
            if (null == user) {
                return new ResponseEntity<>(messageRes, HttpStatus.BAD_GATEWAY);
            }

            Person findById = userPersonRepository.findByIdAndStatus(req.getId(), Constants.ACTIVE_STATUS);
            if (null == findById) {
                return new ResponseEntity<>(messageRes, HttpStatus.BAD_REQUEST);
            }
            req.setStatus(Constants.DELETE_STATUS);
            req.setCreateAt(new Date());
            req.setCreateBy(Constants.SYSTEM);
            req.setUpdateAt(new Date());
            req.setUpdateBy(Constants.SYSTEM);
            userPersonRepository.save(req);
            messageRes.setMessageSuccess(null);

        } catch (Throwable e) {
            log.info("While error user update user business", e);
        } finally {
            log.info("Final user update user business  response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

}
