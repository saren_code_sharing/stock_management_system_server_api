package com.dinsaren.stockmanagementsystemserverapi.controllers.rest;

import com.dinsaren.stockmanagementsystemserverapi.constants.Constants;
import com.dinsaren.stockmanagementsystemserverapi.models.UnitType;
import com.dinsaren.stockmanagementsystemserverapi.payload.response.MessageRes;
import com.dinsaren.stockmanagementsystemserverapi.repository.UnitTypeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/app/unitType")
@Slf4j
@PreAuthorize("hasRole('USER') or hasRole('CUSTOMER') or hasRole('ADMIN')")
public class UnitTypeController {
    private final UnitTypeRepository unitTypeRepository;
    private MessageRes messageRes;

    public UnitTypeController(UnitTypeRepository unitTypeRepository) {
        this.unitTypeRepository = unitTypeRepository;
    }

    @GetMapping("/list")
    public ResponseEntity<MessageRes> getList() {
        messageRes = new MessageRes();
        List<UnitType> list = new ArrayList<>();
        try {
            log.info("Intercept RMS get all UnitType");
            list = unitTypeRepository.findAllByStatus(Constants.ACTIVE_STATUS);
            messageRes.setMessageSuccess(list);
        } catch (Throwable e) {
            log.info("While error RMS get all UnitType ", e);
        } finally {
            log.info("Final RMS get info response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MessageRes> getList(@PathVariable("id") Integer id) {
        messageRes = new MessageRes();
        try {
            log.info("Intercept RMS get all UnitType");
            Optional<UnitType> UnitType = unitTypeRepository.findByIdAndStatus(id, Constants.ACTIVE_STATUS);
            UnitType.ifPresent(value -> messageRes.setMessageSuccess(value));
        } catch (Throwable e) {
            log.info("While error RMS get all UnitType ", e);
        } finally {
            log.info("Final RMS get info response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<MessageRes> create(@RequestBody UnitType req) {
        messageRes = new MessageRes();
        try {
            log.info("Intercept RMS create UnitType {}", req);
            req.setStatus(Constants.ACTIVE_STATUS);
            req.setId(0);
            req.setCreateAt(new Date());
            req.setCreateBy(Constants.SYSTEM);
            req.setUpdateAt(new Date());
            req.setUpdateBy(Constants.SYSTEM);
            unitTypeRepository.save(req);
            messageRes.setMessageSuccess("Create Success");
        } catch (Throwable e) {
            log.info("While error RMS create UnitType ", e);
        } finally {
            log.info("Final RMS create UnitType response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<MessageRes> update(@RequestBody UnitType req) {
        messageRes = new MessageRes();
        try {

            if (checkRequestBody(req)) return new ResponseEntity<>(messageRes, HttpStatus.BAD_REQUEST);
            log.info("Intercept RMS update UnitType {}", req);
            req.setStatus(Constants.ACTIVE_STATUS);
            req.setCreateAt(new Date());
            req.setCreateBy(Constants.SYSTEM);
            req.setUpdateAt(new Date());
            req.setUpdateBy(Constants.SYSTEM);
            unitTypeRepository.save(req);
            messageRes.setMessageSuccess("update Success");
        } catch (Throwable e) {
            log.info("While error RMS update UnitType ", e);
        } finally {
            log.info("Final RMS update UnitType response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/delete")
    public ResponseEntity<MessageRes> delete(@RequestBody UnitType req) {
        messageRes = new MessageRes();
        try {
            if (checkRequestBody(req)) return new ResponseEntity<>(messageRes, HttpStatus.BAD_REQUEST);
            log.info("Intercept RMS delete UnitType {}", req);
            req.setStatus(Constants.DELETE_STATUS);
            req.setCreateAt(new Date());
            req.setCreateBy(Constants.SYSTEM);
            req.setUpdateAt(new Date());
            req.setUpdateBy(Constants.SYSTEM);
            unitTypeRepository.save(req);
            messageRes.setMessageSuccess("delete Success");
        } catch (Throwable e) {
            log.info("While error RMS delete UnitType ", e);
        } finally {
            log.info("Final RMS delete UnitType response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    private boolean checkRequestBody(@RequestBody UnitType req) {
        if (req.getId() < 0 || req.getName().equals("") || null == req.getName()) {
            messageRes.badRequest("Error: Invalid request!");
            return true;
        }
        Optional<UnitType> UnitType = unitTypeRepository.findByIdAndStatus(req.getId(), Constants.ACTIVE_STATUS);
        if (!UnitType.isPresent()) {
            messageRes.badRequest("Error: Data not found!");
            return true;
        }
        return false;
    }


}
