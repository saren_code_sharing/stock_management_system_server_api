package com.dinsaren.stockmanagementsystemserverapi.controllers.rest;

import com.dinsaren.stockmanagementsystemserverapi.constants.Constants;
import com.dinsaren.stockmanagementsystemserverapi.models.Bank;
import com.dinsaren.stockmanagementsystemserverapi.payload.response.MessageRes;
import com.dinsaren.stockmanagementsystemserverapi.repository.BankRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/app/bank")
@Slf4j
@PreAuthorize("hasRole('USER') or hasRole('CUSTOMER') or hasRole('ADMIN')")
public class BankController {
    private final BankRepository bankRepository;
    private MessageRes messageRes;

    public BankController(BankRepository bankRepository) {
        this.bankRepository = bankRepository;
    }

    @GetMapping("/list")
    public ResponseEntity<MessageRes> getList() {
        messageRes = new MessageRes();
        List<Bank> list = new ArrayList<>();
        try {
            log.info("Intercept RMS get all bank");
            list = bankRepository.findAllByStatus(Constants.ACTIVE_STATUS);
            messageRes.setMessageSuccess(list);
        } catch (Throwable e) {
            log.info("While error RMS get all bank ", e);
        } finally {
            log.info("Final RMS get info response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MessageRes> getList(@PathVariable("id") Integer id) {
        messageRes = new MessageRes();
        try {
            log.info("Intercept RMS get all bank");
            Optional<Bank> object = bankRepository.findByIdAndStatus(id, Constants.ACTIVE_STATUS);
            object.ifPresent(value -> messageRes.setMessageSuccess(value));
        } catch (Throwable e) {
            log.info("While error RMS get all bank ", e);
        } finally {
            log.info("Final RMS get info response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<MessageRes> create(@RequestBody Bank req) {
        messageRes = new MessageRes();
        try {
            log.info("Intercept RMS create bank {}", req);
            req.setStatus(Constants.ACTIVE_STATUS);
            req.setId(0);
            req.setCreateAt(new Date());
            req.setCreateBy(Constants.SYSTEM);
            req.setUpdateAt(new Date());
            req.setUpdateBy(Constants.SYSTEM);
            bankRepository.save(req);
            messageRes.setMessageSuccess("Create Success");
        } catch (Throwable e) {
            log.info("While error RMS create bank ", e);
        } finally {
            log.info("Final RMS create bank response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<MessageRes> update(@RequestBody Bank req) {
        messageRes = new MessageRes();
        try {
            Optional<Bank> find = bankRepository.findById(req.getId());
            if(find.isPresent()) {
                if (checkRequestBody(req)) return new ResponseEntity<>(messageRes, HttpStatus.BAD_REQUEST);
                log.info("Intercept RMS update bank {}", req);
                req.setStatus(Constants.ACTIVE_STATUS);
                req.setCreateAt(new Date());
                req.setCreateBy(Constants.SYSTEM);
                req.setUpdateAt(new Date());
                req.setUpdateBy(Constants.SYSTEM);
                bankRepository.save(req);
            }

            messageRes.setMessageSuccess("update Success");
        } catch (Throwable e) {
            log.info("While error RMS update bank ", e);
        } finally {
            log.info("Final RMS update bank response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    @PostMapping("/delete")
    public ResponseEntity<MessageRes> delete(@RequestBody Bank req) {
        messageRes = new MessageRes();
        try {
            if (checkRequestBody(req)) return new ResponseEntity<>(messageRes, HttpStatus.BAD_REQUEST);
            log.info("Intercept RMS delete bank {}", req);
            req.setStatus(Constants.DELETE_STATUS);
            req.setCreateAt(new Date());
            req.setCreateBy(Constants.SYSTEM);
            req.setUpdateAt(new Date());
            req.setUpdateBy(Constants.SYSTEM);
            bankRepository.save(req);
            messageRes.setMessageSuccess("delete Success");
        } catch (Throwable e) {
            log.info("While error RMS delete bank ", e);
        } finally {
            log.info("Final RMS delete bank response");
        }
        return new ResponseEntity<>(messageRes, HttpStatus.OK);
    }

    private boolean checkRequestBody(@RequestBody Bank req) {
        if (req.getId() < 0 || req.getName().equals("") || null == req.getName()) {
            messageRes.badRequest("Error: Invalid request!");
            return true;
        }
        Optional<Bank> bank = bankRepository.findByIdAndStatus(req.getId(), Constants.ACTIVE_STATUS);
        if (!bank.isPresent()) {
            messageRes.badRequest("Error: Data not found!");
            return true;
        }
        return false;
    }


}
