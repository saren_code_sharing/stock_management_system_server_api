package com.dinsaren.stockmanagementsystemserverapi.repository;

import com.dinsaren.stockmanagementsystemserverapi.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PersonRepository extends JpaRepository<Person, Integer> {
    List<Person> findAllByPersonTypeAndStatus(String type, String status);

    Person findByIdAndStatus(Integer id, String status);

    Optional<Person> findByCheckDefaultAndPersonType(String checkDefault, String type);

    int countByStatusAndPersonType(String status, String type);
}
