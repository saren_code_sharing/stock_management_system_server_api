package com.dinsaren.stockmanagementsystemserverapi.repository;
import com.dinsaren.stockmanagementsystemserverapi.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
    List<Category> findAllByStatus(String status);
    Category findByIdAndStatus(Integer id, String status);
}
