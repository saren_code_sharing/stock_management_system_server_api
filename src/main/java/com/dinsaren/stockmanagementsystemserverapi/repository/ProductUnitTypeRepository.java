package com.dinsaren.stockmanagementsystemserverapi.repository;

import com.dinsaren.stockmanagementsystemserverapi.models.ProductUnit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProductUnitTypeRepository extends JpaRepository<ProductUnit, Integer> {
    List<ProductUnit> findAllByProductIdAndStatus(Integer productId, String status);

    Optional<ProductUnit> findByIdAndStatus(Integer id, String status);

    Optional<ProductUnit> findByProductIdAndUnitTypeId(Integer productId, Integer uniTypeId);

    List<ProductUnit> findAllByStatusOrderByProductId(String status);

    int countByStatus(String status);

}
