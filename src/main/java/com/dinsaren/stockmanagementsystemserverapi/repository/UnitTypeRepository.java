package com.dinsaren.stockmanagementsystemserverapi.repository;

import com.dinsaren.stockmanagementsystemserverapi.models.UnitType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UnitTypeRepository extends JpaRepository<UnitType, Integer> {
    List<UnitType> findAllByStatus(String status);

    Optional<UnitType> findByIdAndStatus(Integer id, String status);

    int countByStatus(String status);
}
