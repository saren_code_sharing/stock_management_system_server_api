package com.dinsaren.stockmanagementsystemserverapi.repository;

import com.dinsaren.stockmanagementsystemserverapi.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
  Optional<User> findByUsernameAndStatus(String username, String status);
  Optional<User> findByPhoneNumberAndStatus(String phone, String status);
  Boolean existsByUsername(String username);
  Boolean existsByEmail(String email);
  Boolean existsByPhoneNumber(String phone);
}
