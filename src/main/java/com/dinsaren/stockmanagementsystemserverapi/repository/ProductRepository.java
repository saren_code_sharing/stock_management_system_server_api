package com.dinsaren.stockmanagementsystemserverapi.repository;

import com.dinsaren.stockmanagementsystemserverapi.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Integer> {
    List<Product> findAllByStatus(String status);

    Optional<Product> findByIdAndStatus(Integer id, String status);

    List<Product> findAllByCategoryIdAndStatus(Integer categoryId, String status);

    int countByStatus(String status);

    List<Product> findAllByDiscountGreaterThanAndStatus(Double discount, String status);

}
