package com.dinsaren.stockmanagementsystemserverapi.repository;

import com.dinsaren.stockmanagementsystemserverapi.models.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface TransactionRepository extends JpaRepository<Transaction, Integer> {
    List<Transaction> findAllByStatusAndTransactionType(String status, String transactionType);

    Page<Transaction> findAllByStatusAndTransactionTypeOrderByIdDesc(String status, String transactionType, Pageable pageable);


    Optional<Transaction> findByIdAndStatus(Integer id, String status);

    Optional<Transaction> findByTransactionSessionAndStatus(String transactionSession, String status);

    List<Transaction> findAllByCreateAtGreaterThanEqualAndCreateAtLessThanEqualOrderByIdDesc(Date startDate, Date endDate);

    List<Transaction> findAllByCreateAtBetweenOrderById(Date startDate, Date endDate);

    List<Transaction> findAllByCreateAtAfterAndCreateAtBefore(Date startDate, Date endDate);

    @Query(value = "select * from rms_transaction t where t.created_at between SYMMETRIC :startDate AND :endDate", nativeQuery = true)
    List<Transaction> findByCreateAtBetweenCreateAt(@Param("startDate") String startDate, @Param("endDate") String endDate);

    int countByStatusAndTransactionType(String status, String transactionType);
}
