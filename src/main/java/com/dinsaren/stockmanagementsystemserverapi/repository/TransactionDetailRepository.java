package com.dinsaren.stockmanagementsystemserverapi.repository;

import com.dinsaren.stockmanagementsystemserverapi.models.TransactionDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TransactionDetailRepository extends JpaRepository<TransactionDetail, Integer> {
    List<TransactionDetail> findAllByTransactionId(Integer transactionId);

    Optional<TransactionDetail> findByIdAndStatus(Integer id, String status);

    Optional<TransactionDetail> findByTransactionIdAndStatusAndProductId(Integer transactionId, String status, Integer productId);
}
