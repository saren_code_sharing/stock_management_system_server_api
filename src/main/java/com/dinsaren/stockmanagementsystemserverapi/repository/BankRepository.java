package com.dinsaren.stockmanagementsystemserverapi.repository;

import com.dinsaren.stockmanagementsystemserverapi.models.Bank;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BankRepository extends JpaRepository<Bank, Integer> {
    List<Bank> findAllByStatus(String status);
    Optional<Bank> findByIdAndStatus(Integer id, String status);
    int countByStatus(String status);
}
