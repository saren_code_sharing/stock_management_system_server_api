package com.dinsaren.stockmanagementsystemserverapi.repository;

import com.dinsaren.stockmanagementsystemserverapi.models.ExchangeRate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ExchangeRateRepository extends JpaRepository<ExchangeRate, Integer> {
    List<ExchangeRate> findAllByStatusOrderByIdDesc(String status);

    Optional<ExchangeRate> findByIdAndStatus(Integer id, String status);

    Optional<ExchangeRate> findTopByOrderByIdDesc();
}
