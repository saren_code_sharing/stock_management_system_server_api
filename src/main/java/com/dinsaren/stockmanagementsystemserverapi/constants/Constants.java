package com.dinsaren.stockmanagementsystemserverapi.constants;

import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Constants {
    public static final String DELETE_STATUS = "DEL";
    public static final String MENU = "MENU";
    public static final String PROMOTION = "PROMOTION";
    public static final String APPLICATION = "APPLICATION";
    public static final String SAVING_ACCOUNT = "SAVING_ACCOUNT";
    public static final String TERM_DEPOSIT_ACCOUNT = "TERM_DEPOSIT_ACCOUNT";
    public static final String TXN_SUC = "SUC";
    public static final String TXN_INIT = "INT";
    public static final String TXN_DEL = "DEL";
    public static final String CUSTOMER = "CUSTOMER";
    public static final String SUPPLIER = "SUPPLIER";
    public static final String CASHIER = "CASHIER";
    public static final String MAIN_TXN = "MAIN_TXN";
    public static String ACTIVE_STATUS = "ACT";
    public static String ACTIVE_INT = "INT";
    public static final String SYSTEM = "SYS";
    public static String YES = "Y";
    public static String NO = "N";

    public static final String STATUS_DEL = "DEL";
    public static final String THIS_WEEK = "THIS_WEEK";
    public static final String THIS_MONTH = "THIS_MONTH";
    public static final String THIS_YEAR = "THIS_YEAR";
    public static final String TODAY = "TODAY";
    public static final String ALL = "ALL";
    public  static  String STATUS_ROOM_ACTIVE = "Active";
    public  static final String STATUS_ROOM_PENDING = "pending";

    public static final String dateFormat = "yyyy-MM-dd";
    public static final String dateTimeFormat = "yyyy-MM-dd HH:mm:ss";

    public static String BOOK_STATUS_INIT = "INT";
    public static String BOOK_STATUS_SUC = "SUC";
    public static String CURRENCY_USD="USD";
    public static String CURRENCY_KHR="KHR";
    public static String ORDER = "ORDER";
    public static String PURCHASE = "PURCHASE";

    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jsonCustomizer() {
        return builder -> {
            builder.simpleDateFormat(dateTimeFormat);
            builder.serializers(new LocalDateSerializer(DateTimeFormatter.ofPattern(dateFormat)));
            builder.serializers(new LocalDateTimeSerializer(DateTimeFormatter.ofPattern(dateTimeFormat)));
        };
    }
    public static Date formatStringDateToDate(String date){
        try {
            return new SimpleDateFormat(dateFormat).parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }

}
